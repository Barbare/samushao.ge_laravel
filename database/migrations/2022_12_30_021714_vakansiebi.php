<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Vakansiebi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vakansiebi', function (Blueprint $table) {
            $table->id();
            $table->string('saxeoba');
            $table->string('damsaqmebeli');
            $table->string('shemotavazeba');
            $table->string('anazgaureba');
            $table->string('mokle_info');
            $table->string('el_posta');
            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vakansiebi');
    }
}
