<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Momxmareblebi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('momxmareblebi', function (Blueprint $table) {
            $table->id();
            $table->string('saxeli');
            $table->string('gvari');
            $table->string('tel_nomeri');
            $table->string('el_fosta');
            $table->string('dab_tarigi');
            $table->string('paroli');
            $table->string('gaimeoret_paroli');
            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('momxmareblebi');
    }
}
