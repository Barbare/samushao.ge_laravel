<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\momxmarebeli_controller;
use App\Http\Controllers\vakansiebi_controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [vakansiebi_controller::class, 'index']);

// Route::get('/mtavari', function () {
//     return view('mtavari');
// });

Route::get('/chvens_shesaxeb', function () {
    return view('chvens_shesaxeb');
});

Route::get('/gamoqveyneba', function () {
    return view('gamoqveyneba');
});

Route::get('/avtorizacia', function () {
    return view('avtorizacia');
});

Route::get('/registracia', function () {
    return view('registracia');
});

Route::post('/registracia', [momxmarebeli_controller::class, 'sheqmna']);
// Route::put('/user/{id}', [momxmarebeli_controller::class, 'sheqmna']);
// Route::post('/registracia', 'RegistrationController@register');

Route::post('/create_vakansia', [vakansiebi_controller::class, 'sheqmna']);


Route::get('/vakansia', function () {
    return view('vakansia');
});

Route::get('/vakansia_axali/{id}', [vakansiebi_controller::class, 'get']);

Route::get('/atvirtva_axali', function () {
    return view('atvirtva_axali');
});

Route::get('/saadmino', [vakansiebi_controller::class, 'index']);
