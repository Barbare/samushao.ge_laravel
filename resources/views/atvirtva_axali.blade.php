<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> chvens shesaxeb </title>
    <link rel="stylesheet" href="{{ asset('css/atvirtva.css') }}">
      <!-- <img src="{{ asset('potoebi/fuuli.png') }}" alt="Fuuli"> -->

</head>
<body>
    
    <div class="gilakebi">
        <a href="{{ url('mtavari') }}" class="previous round a_hover">&#8249;</a>

        <label class="switch">
            <input type="checkbox" onclick="d_mode()">
            <!-- <span class="slider round"></span> -->
        </label>
    </div>

    <!-- <form class="d0" action="../mtavari/mtavari.php">  -->
        <form action="/create_vakansia" method="POST" class="d0">
            @csrf
            <div class="f0" style="display: none;">
                <label for="saxeoba">სახეობა</label>
                <input type="text" class="f1" placeholder="დამსაქმებელი" name="saxeoba" value="{{ app('request')->input('saxeoba') }}">
            </div>

            <div class="f0">
                <label for="damsaqmebeli">დამსაქმებელი</label>
                <input type="text" class="f1" placeholder="დამსაქმებელი" name="damsaqmebeli">
            </div>

            <div class="f0">
                <label for="shemotavazeba">შემოთავაზება</label>
                <input type="text" class="f1" placeholder="შემოთავაზება" name="shemotavazeba">
            </div>

            <div class="f0">
                <label for="anazgaureba">ანაზღაურება</label>
                <input type="text" class="f1" placeholder="ანაზღაურება" name="anazgaureba">
            </div>

            <div class="f0">
                <label for="mokle_info">მოკლე ინფო</label>
                <input type="text" class="f1" placeholder="მოკლე ინფო" name="mokle_info">
            </div>

            <div class="f0">
                <label for="el_posta">ელ_ფოსტა</label>
                <input type="text" class="f1" placeholder="ელ_ფოსტა" name="el_posta">
            </div>
            
            <br>

            <div class="f0">
                <a href="{{ url('vakansia') }}"><input type="submit" class="b1" value="გამოქვეყნება" name="gamoqveyneba"></a>
            </div>
    </form>

            
        </div>
    </form>

<script src="{{ asset('js/mtavari.js') }}"></script>

