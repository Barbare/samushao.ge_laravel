<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> chveni vakansia </title>
    <link rel="stylesheet" href="{{ asset('css/mtavari.css') }}">
      <!-- <img src="{{ asset('potoebi/fuuli.png') }}" alt="Fuuli"> -->

</head>
<body>

    <!-- menuebi -->
  <!-- ==================================================== -->
  <div class="d0">

      <!-- satauri -->
  <!-- ---------------------------------------------------- -->
      <div class="dasaxeleba"><a href="{{ url('/') }}"> SAMUSHAO.GE </a></div>
  <!-- ---------------------------------------------------- -->


      <!-- kompis menu -->
  <!-- ---------------------------------------------------- -->
  <div class="k_menu">
    <ul>
      <li><a href="{{ url('/') }}">მთავარი</a></li>
      <!-- <li><a href="../chvens shesaxeb/chvens_shesaxeb.php">ჩვენს შესახებ</a></li> -->

      <li><a href="{{ url('chvens_shesaxeb') }}">ჩვენს შესახებ</a></li>
      <li><a href="{{ url('gamoqveyneba') }}"> გამოქვეყნება </a></li>
      <li class="log_in"><a href="{{ url('avtorizacia') }}" >ავტორიზაცია</a></li>
      <li class="sign_up"><a href="{{ url('registracia') }}">რეგისტრაცია</a></li>
      <br>
      <label class="switch">
        <li class="no_border">
          <input type="checkbox" onclick="d_mode()">
          <div class="slider round"></div>
        </li>
      </label>
    </ul>
  </div>
  <!-- ---------------------------------------------------- -->


      <!-- mobiluris menu -->
  <!-- ---------------------------------------------------- -->
  <div class="h_icon" onclick="gamoweva(this)">
        <div class="h_1"></div>
        <div class="h_2"></div>
        <div class="h_3"></div>
        <ul class="m_menu">
          <li><a href="{{ url('/') }}"> მთავარი </a></li>
          <li><a href="{{ url('chvens_shesaxeb') }}">ჩვენს შესახებ</a></li>
          <li><a href="{{ url('gamoqveyneba') }}"> გამოქვეყნება </a></li>
          <li><a href="{{ url('avtorizacia') }}" >ავტორიზაცია</a></li>
          <li><a href="{{ url('registracia') }}">რეგისტრაცია</a></li>
          <br>
          <label class="switch">
            <li>
              <input type="checkbox" onclick="d_mode()">
              <!-- <div class="slider round"></div> -->
            </li>
          </label>
        </ul>
      </div>
  <!-- ---------------------------------------------------- -->
    </div>
  <!-- ==================================================== -->



  <table class="sia_0">

<tr class="sia, th_h">
  <th class="sia_tito">შემოთავაზება</th>
  <th class="sia_tito">დამსაქმებელი</th>
  <th class="sia_tito">ანაზღაურება</th>
</tr>

@foreach($vakansiebi as $vakansia)
@if($vakansia->saxeoba == 'super_vip')
<tr style="color:black">
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->shemotavazeba }}</a></td>
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->damsaqmebeli }}</a></td>
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->anazgaureba }}</a></td>
</tr>
@endif
@endforeach

</table>

<table class="sia_0">

<tr class="sia, th_h">
  <th class="sia_tito">შემოთავაზება</th>
  <th class="sia_tito">დამსაქმებელი</th>
  <th class="sia_tito">ანაზღაურება</th>
</tr>

@foreach($vakansiebi as $vakansia)
@if($vakansia->saxeoba == 'vip')
<tr style="color:black">
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->shemotavazeba }}</a></td>
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->damsaqmebeli }}</a></td>
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->anazgaureba }}</a></td>
</tr>
@endif
@endforeach

</table>

<table class="sia_0">

<tr class="sia, th_h">
  <th class="sia_tito">შემოთავაზება</th>
  <th class="sia_tito">დამსაქმებელი</th>
  <th class="sia_tito">ანაზღაურება</th>
  <th class="sia_tito">მოკლე ინფორმაცია</th>
  <th class="sia_tito">ელექტრონული ფოსტა</th>

</tr>

@foreach($vakansiebi as $vakansia)
@if($vakansia->saxeoba == 'ubralo')
<tr style="color:black">
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->saxeoba }}</a></td>
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->shemotavazeba }}</a></td>
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->damsaqmebeli }}</a></td>
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->anazgaureba }}</a></td>
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->mokle_info }}</a></td>
  <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->el_posta }}</a></td>

</tr>
@endif
@endforeach

</table>

<script src="{{ asset('js/mtavari.js') }}"></script>








