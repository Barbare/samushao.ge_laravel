<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> chvens shesaxeb </title>
    <link rel="stylesheet" href="{{ asset('css/registracia.css') }}">
      <!-- <img src="{{ asset('potoebi/fuuli.png') }}" alt="Fuuli"> -->

</head>
<body>

    <div class="gilakebi">
        <a href="{{ url('/') }}" class="previous round a_hover">&#8249;</a>

        <label class="switch">
            <input type="checkbox" onclick="d_mode()">
        </label>
    </div>

    <form action="/registracia" method="POST">
        @csrf
        <div class="d0">
            <div class="t1">რეგისტრაცია</div>

            <div class="d1">
                <div class="f0">
                    <label for="saxeli">სახელი</label>
                    <input type="text" class="f1" placeholder="სახელი"  name="saxeli" required>
                </div>

                <div class="f0">
                    <label for="gvari">გვარი</label>
                    <input type="text" class="f1" placeholder="გვარი" name="gvari" required>
                </div>

                <div class="f0">
                    <label for="">ტელ ნომერი</label>
                    <input type="text" class="f1" placeholder="ტელ ნომერი" name="tel_nomeri" required>
                </div>

                <div class="f0 chasaweri" id="fosta_id" >
                    <label for="">ელ ფოსტა</label>
                    <input type="email" class="f1" placeholder="ელ ფოსტა" id="fosta_chasaweri" onkeydown="fosta_shemowmeba()" name="el_fosta">
                </div>

                <div class="f0" required>
                    <label for="">დაბადების თარღი</label>
                    <input type="date" class="f1" placeholder="დაბადების თარღი" name="dab_tarigi">
                </div>

                <div class="f0" required>
                    <label for="">პაროლი</label>
                    <input type="password" class="f1" placeholder="პაროლი" name="paroli" required>
                </div>

                <div class="f0" required>
                    <label for="">გაიმეორეთ პაროლი</label>
                    <input type="password" class="f1" placeholder="გაიმეორეთ პაროლი" name="gaimeoret_paroli" required>
                </div>

                <div class="f0">
                    <a href="{{ url('avtorizacia') }}"><input type="submit" class="b1" value="რეგისტრაცია" name="dadastureba"></a>
                </div>

            </div>
        </div>
    </form>


<script src="{{ asset('js/mtavari.js') }}"></script>