<html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>mtavari</title>
      <link rel="stylesheet" href="{{ asset('css/mtavari.css') }}">
      <!-- <img src="{{ asset('potoebi/fuuli.png') }}" alt="Fuuli"> -->

    </head>
    <body>

    </head>
<body>

<body>

    <!-- menuebi -->
<!-- ==================================================== -->
    <div class="d0">

      <!-- satauri -->
<!-- ---------------------------------------------------- -->
      <div class="dasaxeleba"><a href="{{ url('/') }}"> SAMUSHAO.GE </a></div>
<!-- ---------------------------------------------------- -->


      <!-- kompis menu -->
<!-- ---------------------------------------------------- -->
      <div class="k_menu">
        <ul>
          <li><a href="{{ url('/') }}">მთავარი</a></li>
          <!-- <li><a href="../chvens shesaxeb/chvens_shesaxeb.php">ჩვენს შესახებ</a></li> -->

          <li><a href="{{ url('chvens_shesaxeb') }}">ჩვენს შესახებ</a></li>
          <li><a href="{{ url('gamoqveyneba') }}"> გამოქვეყნება </a></li>
          <li class="log_in"><a href="{{ url('avtorizacia') }}" >ავტორიზაცია</a></li>
          <li class="sign_up"><a href="{{ url('registracia') }}">რეგისტრაცია</a></li>
          <br>
          <label class="switch">
            <li class="no_border">
              <input type="checkbox" onclick="d_mode()">
              <div class="slider round"></div>
            </li>
          </label>
        </ul>
      </div>
<!-- ---------------------------------------------------- -->


      <!-- mobiluris menu -->
<!-- ---------------------------------------------------- -->
      <div class="h_icon" onclick="gamoweva(this)">
        <div class="h_1"></div>
        <div class="h_2"></div>
        <div class="h_3"></div>
        <ul class="m_menu">
          <li><a href="{{ url('/') }}">მთავარი</a></li>
          <li><a href="{{ url('chvens_shesaxeb') }}">ჩვენს შესახებ</a></li>
          <li><a href="{{ url('gamoqveyneba') }}"> გამოქვეყნება </a></li>
          <li><a href="{{ url('avtorizacia') }}" >ავტორიზაცია</a></li>
          <li><a href="{{ url('registracia') }}">რეგისტრაცია</a></li>
          <br>
          <label class="switch">
            <li>
              <input type="checkbox" onclick="d_mode()">
              <div class="slider round"></div>
            </li>
          </label>
        </ul>
      </div>
<!-- ---------------------------------------------------- -->
    </div>
<!-- ==================================================== -->



      <!-- super vip vakansiebi -->
<!-- ==================================================== -->
    <div class="slaideri_container">

    <div class="slaideri fade">
      <a href="../vakansia/vakansia.php"><img src="./potoebi/ofoodo.jpg" class="slaideri_poto"></a>
      <div class="slaideri_text">არგაუშვათ არაჩვეულებრივი ვაკანსია ხელიდან</div>
    </div>

    <div class="slaideri fade">
      <a href="../vakansia/vakansia.php"></a><img src="./potoebi/raddison.jpg" class="slaideri_poto"></a>
      <div class="slaideri_text">არგაუშვათ არაჩვეულებრივი ვაკანსია ხელიდან</div>
    </div>

    <div class="slaideri fade">
      <a href="../vakansia/vakansia.php"></a><img src="./potoebi/glovo.jpeg" class="slaideri_poto"></a>
      <div class="slaideri_text">არგაუშვათ არაჩვეულებრივი ვაკანსია ხელიდან</div>
    </div>

    </div>
    <br>

    <div>
      <div class="wertilebi"></div> 
      <div class="wertilebi"></div> 
      <div class="wertilebi"></div> 
    </div>
<!-- ==================================================== -->




    <!-- vip vakansiebi -->
<!-- ==================================================== -->
<!-- ---------------------------------------------------- -->
    <table class="sia_0">

      <tr class="sia, th_h">
        <th class="sia_tito">შემოთავაზება</th>
        <th class="sia_tito">დამსაქმებელი</th>
        <th class="sia_tito">ანაზღაურება</th>
      </tr>

      <!-- <tr class="sia">
        <td class="sia_tito"><a href="{{ url('vakansia_axali') }}"> მოლარე </a></td>
        <td class="sia_tito"><a href="{{ url('vakansia_axali') }}"> ორი ნაბიჯი </a></td>
        <td class="sia_tito"><a href="{{ url('vakansia_axali') }}"> 1200 ლარი </a></td>
      </tr> -->

      @foreach($vakansiebi as $vakansia)
      @if($vakansia->saxeoba == 'vip')
      <tr style="color:black">
        <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->shemotavazeba }}</a></td>
        <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->damsaqmebeli }}</a></td>
        <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->anazgaureba }}</a></td>
      </tr>
      @endif
      @endforeach

    </table>
<!-- ---------------------------------------------------- -->
<!-- ==================================================== -->



    <!-- bevr vakansiani -->
<!-- ==================================================== -->
<!-- ---------------------------------------------------- -->
    <div class="damsaqmebeli_0">
      <div class="damsaqmebeli_1">
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/mc.png" alt=""> </a></div>
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/ronnys.png" alt=""> </a></div>
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/spar.png" alt=""> </a></div>
      </div>

      <div class="damsaqmebeli_2">
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/wendy.png" alt=""> </a></div>
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/localino.png" alt=""> </a></div>
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/goodwill.jpeg" alt=""> </a></div>
      </div>

      <div class="damsaqmebeli_3">
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/kfc.png" alt=""> </a></div>
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/bog.png" alt=""> </a></div>
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/karfuri.png" alt=""> </a></div>
      </div>

      <div class="damsaqmebeli_4">
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/dankini.png" alt=""> </a></div>
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/tbc.png" alt=""> </a></div>
        <div class="damsaqmebeli"><a href="../vakansia/vakansia.php"> <img src="./potoebi/nikora.png" alt=""> </a></div>
      </div>
    </div>
<!-- ---------------------------------------------------- -->
<!-- ==================================================== -->


    
    <!-- prosta sia -->
<!-- ==================================================== -->
<!-- ---------------------------------------------------- -->
    <table class="sia_0">

      <tr class="sia, th_h">
        <th class="sia_tito">შემოთავაზება</th>
        <th class="sia_tito">დამსაქმებელი</th>
        <th class="sia_tito">ანაზღაურება</th>
      </tr>

      <!-- <tr class="sia">
        <td class="sia_tito"><a href="../vakansia/vakansia.php"> მოლარე </a></td>
        <td class="sia_tito"><a href="../vakansia/vakansia.php"> ორი ნაბიჯი </a></td>
        <td class="sia_tito"><a href="../vakansia/vakansia.php"> 1200 ლარი </a></td>
      </tr> -->

      @foreach($vakansiebi as $vakansia)
      @if($vakansia->saxeoba == 'ubralo')
      <tr style="color:black">
        <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->shemotavazeba }}</a></td>
        <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->damsaqmebeli }}</a></td>
        <td class="sia_tito"><a href="{{ url('vakansia_axali/'.$vakansia->id) }}">{{ $vakansia->anazgaureba }}</a></td>
      </tr>
      @endif
      @endforeach

    </table>
<!-- ---------------------------------------------------- -->
<!-- ==================================================== -->
  
  <script src="{{ asset('js/mtavari.js') }}"></script>
  
    </body>
  </html>