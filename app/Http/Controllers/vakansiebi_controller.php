<?php

namespace App\Http\Controllers;

use App\Models\vakansiebi_model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class vakansiebi_controller extends Controller
{
    public function sheqmna () {
        // $input = request()->except('_token');
        $vakansia = new vakansiebi_model();

        $vakansia->saxeoba = request()->saxeoba;
        $vakansia->damsaqmebeli = request()->damsaqmebeli;
        $vakansia->shemotavazeba = request()->shemotavazeba;
        $vakansia->anazgaureba = request()->anazgaureba;
        $vakansia->mokle_info = request()->mokle_info;
        $vakansia->el_posta = request()->el_posta;
        // $vakansia->el_posta = "rame@gmail.com";

        $vakansia->save();

        // Log::info(request()->saxeli);
    }

    public function index () {
        $vakansiebi = vakansiebi_model::all();
        return view('/mtavari', ["vakansiebi"=>$vakansiebi]);
    } 

    public function get () {
        // $id = request()->input('id');
        $id = request()->id;
        // Log::info($id);
        // dd($id);
        $vakansia = vakansiebi_model::find($id);
        return view('vakansia_axali', ["vakansia"=>$vakansia]);
    }
}
